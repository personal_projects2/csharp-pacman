﻿using System.Collections.Generic;
using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.AI
{
    public class AiClyde: IAi
    {
        public AiClyde(string name = "Clyde")
        {
            Name = name;
        }

        public string Name { get; set; }
        public Sides NextAttackMove(List<Cell>[,] gameField)
        {
            throw new System.NotImplementedException();
        }

        public Sides NextFleeMove(List<Cell>[,] gameField)
        {
            throw new System.NotImplementedException();
        }

    }
}