﻿using Pacman.Model.GameModel.Actors;

namespace Pacman.Model.GameModel
{
    public class Player
    {
        public int Points { get; set; }
        private GameState _state;

        public Player(GameState state)
        {
            Points = 0;
            _state = state;
        }
        public void EatEntity(object sender, int points)
        {
            Points += points;
            if (sender is Pill) _state.ActivatePill();
        }
    }
}