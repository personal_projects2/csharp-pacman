﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pacman.Model.Engine;
using Pacman.Model.Engine.Entities;
using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Ghost : MovingEntity, IEdible<int>
    {
        public Cell Label { get; set; }
        private IAi _ghostAI;
        private Coord Spawn { get; set; }
        public int Points { get; set; }
        public event EventHandler<int> OnEat;
        public bool Dead { get; set; }
        public bool Attacking { get; set; }
        private List<Sides> _currentPath;
        public Ghost(IAi ghostAi, Coord position, int points = 10): base(position)
        {
            Points = points;
            _ghostAI = ghostAi;
            Attacking = true;
            Label = Cell.Ghost;
        }
        // подписаться на MakeMoves() у GameState ???
        public void NextMoveEvent(object sender, List<Cell>[,] args)
        {
            if (Dead)
            {
                if (_currentPath.Count == 0)
                    _currentPath = args.AStar(Position, Spawn);
                Direction = _currentPath.First();
                _currentPath.RemoveAt(0);
            }
            else if (Attacking)
                Direction = _ghostAI.NextAttackMove(args);
            else
                Direction = _ghostAI.NextFleeMove(args);
            base.Move();
        }

        public override void Collide<TP>(IColliding<TP> another) {}
        public void Collide(Pacman pacman)
        {
            if (!Attacking)
            {
                Dead = true;
            }
        }

        public void FleeMode()
        {
            Attacking = false;
        }

        public void AttackMode()
        {
            Attacking = true;
        }

    }
}