﻿using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Entities;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Coin : StaticEdible<int>
    {
        public Coin(Coord position, Cell label = Cell.Coin, int points = 10) : base(position, label, points)
        {
            Points = points;
        }

        public override void Collide<TP>(IColliding<TP> another){}

        public int Points { get; set; }
        public Cell Label { get; set; }
    }
}