﻿using Pacman.Model.Engine;
using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Entities;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Fruit : StaticEdible<int>, ILabeled
    {
        public Fruit(Coord position, Cell label = Cell.Fruit, int points = 100) : base(position, label, points)
        {
        }

        public override void Collide<TP>(IColliding<TP> another)
        {
            throw new System.NotImplementedException();
        }

        public Cell Label { get; set; }
    }
}