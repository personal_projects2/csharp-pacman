﻿using System;
using Pacman.Model.Engine;
using Pacman.Model.Engine.Entities;
using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Pacman: MovingEntity, ILabeled
    {
        public Pacman(Coord position) : base(position)
        {
            Label = Cell.Pacman;
        }
        public Cell Label { get; set; }
        public override void Collide<TP>(IColliding<TP> another) {}
        public event EventHandler OnDeath;
        public event EventHandler<Entity> OnEat;

        public void Collide(Ghost ghost)
        {
            if (ghost.Dead) return;
            if (ghost.Attacking)
            {
                if (OnDeath != null) OnDeath(this, EventArgs.Empty);
            }
            else if (OnEat != null) OnEat(this, ghost);
        }

    }
}