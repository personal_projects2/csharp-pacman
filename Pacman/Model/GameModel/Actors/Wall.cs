﻿using Pacman.Model.Engine;
using Pacman.Model.Engine.Entities;
using Pacman.Model.Engine.Interfaces;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Wall : StaticEntity
    {
        public Wall(Coord position) : base(position)
        {
        }

        public override void Collide<TP>(IColliding<TP> another)
        {
            throw new System.NotImplementedException();
        }
    }
}