﻿using Pacman.Model.Engine;
using Pacman.Model.Engine.Interfaces;
using Pacman.Model.GameModel.Entities;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Actors
{
    public class Pill : StaticEdible<int>, ILabeled
    {
        public Cell Label { get; set; }

        public Pill(Coord position, Cell label = Cell.Pill, int points = 50) : base(position, label, points)
        {
        }

        public override void Collide<TP>(IColliding<TP> another)
        {
        }

        public void Collide(Pacman another)
        {
        }
    }
}