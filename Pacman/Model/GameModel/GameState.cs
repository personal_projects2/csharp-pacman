﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pacman.Model.Engine;
using Pacman.Model.Engine.Entities;
using Pacman.Model.GameModel.Actors;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel
{
    public class GameState
    {
        private Player _player;
        private List<Cell>[,] _gameField;
        public event EventHandler<List<Cell>[,]> MakeMoves;
        private List<Entity> _entities = new List<Entity>();

        public GameState(Cell[,] field)
        {
            var height = field.GetLength(0);
            var width = field.GetLength(1);
            _gameField = new List<Cell>[height, width];
            for (var y = 0; y < field.GetLength(0); y++)
            {
                for (var x = 0; x < field.GetLength(1); x++)
                {
                    var currentCell = field[y, x];
                    _gameField[y, x] = new List<Cell> {currentCell};
                    if (currentCell == Cell.Ghost)
                        _gameField[y, x].Add(Cell.GhostSpawn);
                }
            }
        }

        private void InitGame()
        {
            for (var y = 0; y < _gameField.GetLength(0); y++)
            {
                for (var x = 0; x < _gameField.GetLength(1); x++)
                {
                    if (_gameField[y, x].Contains(Cell.Ghost))
                    {
                        var ghost = new Ghost(LoadAi.GetAi(), new Coord(x, y));
                        MakeMoves += ghost.NextMoveEvent;
                        _entities.Add(ghost);
                    }
                }
            }

            foreach (var edible in _entities.OfType<IEdible<int>>())
            {
                edible.OnEat += _player.EatEntity;
                edible.OnEat += OnEntityEaten;
            }
        }

        public void OnEntityEaten(object sender, int args)
        {
            var entityToRemove = sender as Entity;
            _entities.Remove(entityToRemove);
            if (entityToRemove != null)
                _gameField[entityToRemove.Position.Y, entityToRemove.Position.X]
                    .Remove(((ILabeled) sender).Label);
        }

        public void ActivatePill()
        {
            var ghosts =
                from entity in _entities
                where entity is Ghost
                select entity as Ghost;
            foreach (var ghost in ghosts)
            {
                ghost.FleeMode();
            }
        }

        private void NextMove()
        {
            MakeMoves?.Invoke(this, _gameField);
            var movingEntities =
                from entity in _entities
                where entity is MovingEntity
                select entity as MovingEntity;
            foreach (var entity in movingEntities)
            {
            }
        }
    }
}