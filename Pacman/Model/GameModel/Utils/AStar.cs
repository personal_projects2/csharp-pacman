﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Utils
{
    public static class Algorithms
    {
        private static void DrawSearchStep(Cell[,] field, HashSet<Coord> closed, List<List<Coord>> open)
        {
            Console.Clear();
            char[,] prF = new char[field.GetLength(0), field.GetLength(1)];
            for (var i = 0; i < field.GetLength(0); i++)
            {
                for (var j = 0; j < field.GetLength(1); j++)
                {
                    if (field[i, j] == Cell.Wall) prF[i, j] = '#';
                    else prF[i, j] = '?';
                }
            }

            foreach (var c in closed) prF[c.Y, c.X] = 'x';
            foreach (var list in open)
            {
                foreach (var c in list) prF[c.Y, c.X] = 'o';
            }

            for (var i = 0; i < field.GetLength(0); i++)
            {
                for (var j = 0; j < field.GetLength(1); j++)
                {
                    Console.Write(prF[i, j]);
                }

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.ReadKey();
        }

        private static List<Coord> RemoveFirst(this List<List<Coord>> list, Func<Coord, Double> comp)
        {
            if (list == null || list.Count == 0)
            {
                return null;
            }

            var search = list.Select(l => l.Last()).ToList();
            var minIndex = 0;
            var min = comp(search[0]);
            for (var i = 0; i < list.Count; i++)
            {
                if (comp(search[i]) < min)
                {
                    min = comp(search[i]);
                    minIndex = i;
                }
            }

            var ret = list[minIndex];
            list.RemoveAt(minIndex);
            return ret;
        }

        public static List<Sides> AStar(this List<Cell>[,] field, Coord from, Coord to)
        {
            var actualField = new Cell[field.GetLength(0), field.GetLength(1)];
            for (var y = 0; y < field.GetLength(0); y++)
            {
                for (var x = 0; x < field.GetLength(1); x++)
                {
                    actualField[y, x] = field[y, x].Contains(Cell.Wall) ? Cell.Wall : Cell.Empty;
                }
            }

            return actualField.AStar(from, to);
        }

        public static List<Sides> AStar(this Cell[,] field, Coord from, Coord to)
        {
            Func<Coord, Double> f = x =>
            {
                var h = x.DistanceTo(from);
                var g = x.DistanceTo(to);
                return h + g;
            };
            var open = new List<List<Coord>>();
            var closed = new HashSet<Coord>();
            var firstList = new List<Coord> {from};
            open.Add(firstList);
            while (open.Count != 0)
            {
                //DrawSearchStep(field, closed, open);
                var p = open.RemoveFirst(f);
                var x = p.Last();
                if (closed.Contains(x)) continue;
                if (x.Equals(to))
                {
                    var ret = new List<Sides>();
                    for (int i = p.Count - 1; i > 0; i--)
                    {
                        ret.Add((p[i] - p[i - 1]).ToSide());
                    }

                    ret.Reverse();
                    return ret;
                }

                closed.Add(x);
                foreach (var side in Enum.GetValues(typeof(Sides)))
                {
                    var neigb = x + ((Sides) side).ToCoord();
                    if (field[neigb.Y, neigb.X] != Cell.Wall && !p.Contains(neigb))
                    {
                        var newPath = new List<Coord>(p);
                        newPath.Add(neigb);
                        open.Add(newPath);
                    }
                }
            }

            return null;
        }
    }
}