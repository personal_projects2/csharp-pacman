﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Utils
{
    public enum Cell
    {
        Empty,
        Wall,
        Ghost,
        Pacman,
        Fruit,
        Pill,
        Coin,
        GhostSpawn
    }

    public static class CellExtensions
    {
        

        public static char ToChar(this Cell cell)
        {
            switch (cell)
            {
                case Cell.Empty:
                    return ' ';
                case Cell.Wall:
                    return '#';
                case Cell.Ghost:
                    return '=';
                case Cell.Pacman:
                    return '\\';
                case Cell.Fruit:
                    return '%';
                case Cell.Pill:
                    return '*';
                case Cell.Coin:
                    return '.';
                case Cell.GhostSpawn:
                    return '?';
                default:
                    return ' ';
            }
        }

        public static Cell ToCell(this char cellChar)
        {
            switch (cellChar)
            {
                case '?':
                    return Cell.GhostSpawn;
                case '#':
                    return Cell.Wall;
                case '.':
                    return Cell.Coin;
                case '*':
                    return Cell.Pill;
                case '=':
                    return Cell.Ghost;
                case '%':
                    return Cell.Fruit;
                case '\\':
                    return Cell.Pacman;
                case ' ':
                    return Cell.Empty;
                default:
                    return Cell.Empty;
            }
        }
    }
}