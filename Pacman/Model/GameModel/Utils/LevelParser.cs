﻿using System;
using System.IO;
using System.Linq;

namespace Pacman.Model.GameModel.Utils
{
    public static class LevelParser
    {
        public static Cell[,] ParseLevel(string filePath)
        {
            try
            {
                var fileLines = File.ReadLines(filePath).ToList();
                var height = fileLines.Count;
                var width = fileLines[0].Length;
                var startingField = new Cell[height, width];
                var j = 0; 
                foreach (var line in fileLines)
                {
                    var i = 0;
                    foreach (var ch in line)
                    {
                        startingField[j, i] = ch.ToCell();
                        i++;
                    }
                    j++;
                }
                return startingField;
            }
            catch (Exception e)
            {
                Console.WriteLine($"File level \"${filePath}\" can't be read. Error: ${e.Message}");
                return null;
            }
        }
    }
}