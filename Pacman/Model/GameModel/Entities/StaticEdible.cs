﻿using System;
using Pacman.Model.Engine.Entities;
using Pacman.Model.GameModel.Interfaces;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.GameModel.Entities
{
    public abstract class StaticEdible<T>: StaticEntity, IEdible<T>
    {
        public int Points { get; set; }
        public event EventHandler<T> OnEat;
        public Cell Label { get; set; }
        public StaticEdible(Coord position, Cell label, int points) : base(position)
        {
            Points = points;
            Label = label;
        }
    }
}