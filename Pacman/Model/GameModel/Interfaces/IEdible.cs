﻿using System;

namespace Pacman.Model.GameModel.Interfaces
{
    public interface IEdible<T>: ILabeled
    {
        public int Points { get; set; }
        public event EventHandler<T> OnEat;
    }
}