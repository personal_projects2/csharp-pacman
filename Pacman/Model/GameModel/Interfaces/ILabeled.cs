﻿using Pacman.Model.GameModel.Utils;

namespace Pacman.Model.GameModel.Interfaces
{
    public interface ILabeled
    {
        public Cell Label { get; set; }
    }
}