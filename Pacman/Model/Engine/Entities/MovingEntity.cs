﻿using System;
using Pacman.Model.Engine.Interfaces;
using Pacman.Utils;

namespace Pacman.Model.Engine.Entities
{
    // сущность с хитбоксом равным точке (хитбокс - это положение сущности в пространстве)


    public abstract class MovingEntity : Entity, IColliding<Coord>, IMoving<int>
    {
        // collision properties
        public event EventHandler OnCollision;
        public Coord Hitbox => Position;

        // movement properties
        private int _cellsToMoveX = 0;
        private int _cellsToMoveY = 0;
        public int Speed { get; set; }
        public Sides Direction { get; set; }
        public event EventHandler OnMove;
        public abstract void Collide<TP>(IColliding<TP> another);


        protected MovingEntity(Coord position, int speed = 1, Sides direction = Sides.Left) : base(position)
        {
            Speed = speed;
            Direction = direction;
        }
        public bool CheckCollision<TP>(IColliding<TP> another)
        {
            if (another == null || !another.Hitbox.Equals(Hitbox)) return false;
            return true;
        }

        /*
         * так как скорость в моей игре может быть больше 1 клетки в секунду
         * а может быть и меньше
         * но коллижены надо обрабатывать при каждом проходе полной клетки
         * мы будем передвигаться не скачками по n клеток
         * а по 1 клетке за раз (но не за секунду)
         * таким образом, пусть скорость равна n.k клеток в секунду (2.5, например)
         * он пройдет за секунду в итоге n(2) раз по 1 клетке и k(1) раз по 0.5
         * криво? извините
         */
        public virtual void Move()
        {
            if (Direction == Sides.Down || Direction == Sides.Up)
                _cellsToMoveY += Speed;
            else
                _cellsToMoveX += Speed;
            while (_cellsToMoveX >= 1 || _cellsToMoveY >= 1)
            {
                OneCellMove();
            }
        }

        protected void OneCellMove()
        {
            if (_cellsToMoveX >= 1)
                _cellsToMoveX -= 1;
            else if (_cellsToMoveY >= 1)
                _cellsToMoveY -= 1;
            Position += Direction.ToCoord();
            OnMove?.Invoke(this, EventArgs.Empty);
        }
    }
}