﻿using System;
using Pacman.Model.Engine.Interfaces;
using Pacman.Utils;

namespace Pacman.Model.Engine.Entities
{
    public abstract class StaticEntity: Entity, IColliding<Coord>
    {
        public event EventHandler OnCollision;
        public Coord Hitbox { get; private set; }
        public bool CheckCollision<TP>(IColliding<TP> another)
        {
            if (another == null || !another.Hitbox.Equals(Hitbox)) return false;
            return true;
        }

        public abstract void Collide<TP>(IColliding<TP> another);

        protected StaticEntity(Coord position) : base(position)
        {
            Hitbox = position;
        }
    }
}