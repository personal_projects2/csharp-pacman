﻿using Pacman.Utils;

namespace Pacman.Model.Engine.Entities
{
    
    public class Entity
    {
        public Coord Position { get; set;  }

        protected Entity(Coord position)
        {
            Position = position;
        }

        // корень из ((x2 - x1)^2 + (y2 - y1)^2)
        protected internal double DistanceTo(Entity another)
        {
            return Position.DistanceTo(another.Position);
        }
    }
}