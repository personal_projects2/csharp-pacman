﻿using System;
using System.Collections.Generic;

namespace Pacman.Utils
{
    public enum Sides
    {
        Left,
        Right,
        Up,
        Down
    }
    public static class SidesExtensions
    {
        public static Sides ToSide(this Coord coord)
        {
            if (coord.X == 1 && coord.Y == 0) return Sides.Right;
            if (coord.X == -1 && coord.Y == 0) return Sides.Left;
            if (coord.X == 0 && coord.Y == 1) return Sides.Down;
            if (coord.X == 0 && coord.Y == -1) return Sides.Up;
            throw new NotSupportedException("this coord is not a direction");
        }
        public static Coord ToCoord(this Sides side)
        {
            switch (side)
            {
                case Sides.Left:
                    return new Coord(-1, 0);
                case Sides.Right:
                    return new Coord(1, 0);
                case Sides.Up:
                    return new Coord(0, -1);
                case Sides.Down:
                    return new Coord(0, 1);
                default:
                    return new Coord(0, 0);
            }
        }
    }
}