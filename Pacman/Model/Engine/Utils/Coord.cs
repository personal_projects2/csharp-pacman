﻿using System;

namespace Pacman.Utils
{
    public class Coord
    {
        public Coord(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; }
        public int Y { get; }

        public static Coord operator*(Coord coord, int mult)
        {
            return new Coord(coord.X * mult, coord.Y * mult);
        }

        public static Coord operator +(Coord lCoord, Coord rCoord)
        {
            return new Coord(lCoord.X + rCoord.X, lCoord.Y + rCoord.Y);
        }

        public static Coord operator +(Coord lCoord, Sides side)
        {
            return lCoord + side.ToCoord();
        }
        public static Coord operator -(Coord lCoord, Coord rCoord)
        {
            return new Coord(lCoord.X - rCoord.X, lCoord.Y - rCoord.Y);
        }

        public override int GetHashCode()
        {
            return X * 10000 + Y;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Coord another)) return false;
            return X == another.X && Y == another.Y;
        }

        public double DistanceTo(Coord another)
        {
            return Math.Sqrt(Math.Pow(another.X -X, 2) + Math.Pow(another.Y - Y, 2));
        }

        public int CompareTo(Coord another)
        {
            if (this.X == another.X) return this.Y - another.Y;
            else return this.X - another.X;
        }
    }
}