﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Pacman.Model;
using Pacman.Model.Engine.Interfaces;

namespace Pacman.Utils
{
    static internal class LoadAi
    {
        private static List<Type> _aiTypes = new List<Type>();
        private static int _position = -1;
        public static IAi GetAi()
        {
            if (_position == -1)
            {
                LoadAssemblys();
            }
            _position += 1;
            if (_position >= _aiTypes.Count)
            {
                _position = 0;
            }
            return Activator.CreateInstance(_aiTypes[_position], BindingFlags.CreateInstance |
                                                       BindingFlags.Public |
                                                       BindingFlags.Instance |
                                                       BindingFlags.OptionalParamBinding, null,
                new[] {Type.Missing}, CultureInfo.CurrentCulture) as IAi;
        }
        private static void LoadAssemblys()
        {
            const string aiDirectory = "C:\\RiderProjects\\Pacman\\Pacman\\AIs";
            var files = Directory.GetFiles(aiDirectory)
                .Select(path => Path.GetFileName(path))
                .Where(fileName => Path.GetExtension(fileName) == ".dll");
            var fileList = files.ToList();
            foreach (var file in fileList)
            {
                Console.WriteLine($"found {file} in AI folder");
            }

            var aiTypes = fileList
                .Select(file => Assembly.LoadFrom($"{aiDirectory}\\{file}"))
                .Select(assembly => assembly.GetTypes().Where(type => typeof(IAi).IsAssignableFrom(type)).ToArray())
                .SelectMany(types => types);
            _aiTypes = aiTypes.ToList();
            if (_aiTypes.Count <= 0)
                throw new DllNotFoundException("No AI DLL's found!");
        }
    }    
}