﻿using System;

namespace Pacman.Model.Engine.Interfaces
{
    public interface IMoving <T>
    {
        T Speed { get; set; }
        event EventHandler OnMove;
        void Move();
    }
}