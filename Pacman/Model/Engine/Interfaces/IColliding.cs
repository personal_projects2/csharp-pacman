﻿using System;
using Pacman.Utils;

namespace Pacman.Model.Engine.Interfaces
{
    public interface IColliding<T>
    {
        event EventHandler OnCollision;
        Coord Hitbox { get;}
        bool CheckCollision<TP>(IColliding<TP> another);
        void Collide<TP>(IColliding<TP> another);
    }
}