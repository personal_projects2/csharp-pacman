﻿using System.Collections.Generic;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman.Model.Engine.Interfaces
{
    public interface IAi
    {
        string Name { get; set; }

        Sides NextAttackMove(List<Cell>[,] gameField);

        Sides NextFleeMove(List<Cell>[,] gameField);
    }
}