﻿using System;
using Pacman.Model.GameModel.Utils;
using Pacman.Utils;

namespace Pacman
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var a = LevelParser.ParseLevel("C:\\RiderProjects\\Pacman\\Pacman\\Levels\\Level_1.txt");
            var start = new Coord(1, 1);
            var end = new Coord(20, 20);
            var b = a.AStar(start, end);
            Console.WriteLine(b);
            char[,] prF = new char[a.GetLength(0), a.GetLength(1)];
            for (var i = 0; i < a.GetLength(0); i++)
            {
                for (var j = 0; j < a.GetLength(1); j++)
                {
                    prF[i,j] = a[i,j] == Cell.Wall ? '#' : 'o';
                }
            }
            var coord = start;
            prF[start.Y, start.X] = 'S';
            foreach (var s in b)
            {
                coord += s;
                prF[coord.Y, coord.X] = '-';
            }
            prF[end.Y, end.X] = 'E';
            Console.Clear();
            for (var i = 0; i < prF.GetLength(0); i++)
            {
                for (var j = 0; j < prF.GetLength(1); j++)
                {
                    Console.Write(prF[i,j]);
                }
                Console.WriteLine();
            }
        }
    }
}